[![pipeline status](https://gitlab.com/mdubrois/pylint-report-converter/badges/master/pipeline.svg)](https://gitlab.com/mdubrois/pylint-report-converter/commits/master)

[![coverage report](https://gitlab.com/mdubrois/pylint-report-converter/badges/master/coverage.svg)](https://md-node_graph.gitlab.io/mdubrois/pylint-report-converter/coverage-python/)


# Pylint Report Converter

This repository converts a Pylint output message into a message in another format.
Originally it was developed to convert a Pylint message to a Code Climate message for Gitlab.