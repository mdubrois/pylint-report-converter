#!/usr/bin/env bash
# ---
# Clean Previous Run
clear
rm -rf ./docs/build/

# ---
# Pimp Env
ORIG_PYTHONPATH=${PYTHONPATH}
export PYTHONPATH=${PYTHONPATH}:${PWD}
echo "| PYTHONPATH : ${PYTHONPATH}"

# Launch Doc Generation
echo "| Create Dir : public"
mkdir -p public
echo "| Go to : ./docs"
cd ./docs
make html
echo "| Copy Docs Html to public"
cp -r ./build/html/* ../public/
cd ..

# ---
# Restore Env for Local Testing
export PYTHONPATH=${ORIG_PYTHONPATH}
