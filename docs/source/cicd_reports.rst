CICD Reports
===============

This page reference the reports generate during the Continous Integration Process.

    - `Code Coverage <https://mdubrois.gitlab.io/pylint-report-converter/test/htmlcov/>`_
    - `Pylint Report <https://mdubrois.gitlab.io/pylint-report-converter/public/quality/pylint_report_python_full.txt>`_