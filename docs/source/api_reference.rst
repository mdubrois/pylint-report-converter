API Reference
===============
This document is for developers, it contains the API functions.

``pylint_report_converter`` Module
------------------------------------


.. automodule:: pylint_report_converter.__main__
    :members:
    :private-members:
    :inherited-members:


``pylint_report_converter`` Module
------------------------------------

.. automodule:: pylint_report_converter.main
    :members:
    :private-members:
    :inherited-members:

Class
----------------
.. That's Autodoc the class

.. autoclass:: pylint_report_converter.main.Converter
    :members:
    :show-inheritance:

Command Line
----------------

The ``pylint_report_converter`` module is also available in command line.
This is particularly useful in a gitlab ci context.

.. code-block:: bash

    pylint_report_converter -i INPUT_FILE -o OUTPUT_FILE [-f {gitlab}]

Below, the help provided by the ``pylint_report_converter -h`` command.

.. code-block:: bash
    :linenos:

    usage: pylint_report_converter [-h] -i INPUT_FILE -o OUTPUT_FILE [-f {gitlab}]

    optional arguments:
    -h, --help            show this help message and exit
    -i INPUT_FILE, --input_file INPUT_FILE
                            The Pylint input file path.
    -o OUTPUT_FILE, --output_file OUTPUT_FILE
                            The output file path.
    -f {gitlab}, --format {gitlab}
                            The output format.
