Pylint Report Converter
======================================

Pylint-Report-Converter is a tool that converts a Pylint output message to another format.

Originally it was developed to convert a Pylint message to the Code-Climate format that is supported by GitLab.

This is particularly useful if you want to have your Pylint feedback in your GitLab Merge Request.


User Guide
---------------

.. toctree::
    :maxdepth: 2

    user_guide
    api_reference
    cicd_report


Indices and tables
--------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
