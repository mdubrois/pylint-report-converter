"""
This file convert the pylint message output to an output format.
"""
import re
import json
import hashlib
import logging

LOGGER = logging.getLogger("pylint_report_converter")
LOGGER.setLevel(logging.INFO)


class Converter(object):
    """
    This Class do the job to convert a pylint message to an other
    format like gitlab code quality format.
    """

    def __init__(self, input_file, output_file, output_format="gitlab"):
        """
        This function init the class and its attributes
        """
        self.input_file = input_file
        self.output_file = output_file
        self.output_format = output_format

        self.input_report = None
        self.internal_report = {}
        self.output_report = None

    def convert(self):
        """
        This function run the main process
        - read the input file
        - convert the report
        - write the new report

        :rtype: str
        :return: return the output file path
        """
        # read file
        self.read_file()

        # convert
        if self.output_format == "gitlab":
            self.convert_to_gitlab()

        # save file
        self.write_report()

        return 0

    def read_file(self):
        """
        This function read the file
        and store the data into ``self.input_report``

        :rtype: str
        :return: The raw data containded in ``self.input_report`` or None
        """
        with open(self.input_file, "r") as input_file:
            LOGGER.info("Read File : {}".format(self.input_file))
            self.input_report = input_file.read()
            return self.input_report
        return None

    def write_report(self):
        """
        This function write and encode the report
        the content is based on `self.output_report`

        :rtype: str
        :return: return a
        """
        report = self.output_report
        if self.output_format == "gitlab":
            report = json.dumps(self.output_report, indent=4)

        with open(self.output_file, "w") as output_file:
            LOGGER.info("Write File : {}".format(self.output_file))
            output_file.write(report)
            return self.output_file
        return None

    def parse_pylint_report(self):
        """
        This function parse the pylint report and pythonise it.

        The pylint report should follow the message convention
        ``{path}:{line}:{column} [{msg_id}({symbol}), {obj}] {msg}``

        It return a dict with the schema below.

        .. code-block:: python

            {
                $path: {
                    $line_number: [
                        {
                            "msg_id": $msg_id,
                            "symbol": $symbol,
                            "obj": $obj,
                            "msg": $msg,
                            "column_number": $column_number
                        }
                    ]
                }
            }

        :rtype: dict
        :return: return a dict
        """
        # Init report
        report = {}
        # Regexp message
        msg_reg = r"(.*):(\d*):(\d*): \[(.\d{4})\((.*)\), (.*)\] (.*)"

        for line in self.input_report.split("\n"):
            match = re.match(msg_reg, line)
            if match:
                if len(re.match(msg_reg, line).groups()) == 7:
                    groups = re.match(msg_reg, line).groups()
                    path = groups[0]
                    line_number = groups[1]
                    column_number = groups[2]
                    msg_id = groups[3]
                    symbol = groups[4]
                    obj = groups[5]
                    msg = groups[6]
                    # init dict
                    if path not in report.keys():
                        report[path] = {}
                        report[path][line_number] = []
                    else:
                        if line_number not in report[path].keys():
                            report[path][line_number] = []
                    # add msg to the report
                    line_msg = {
                        "msg_id": msg_id,
                        "symbol": symbol,
                        "obj": obj,
                        "msg": msg,
                        "column_number": column_number,
                    }
                    report[path][line_number].append(line_msg)
        self.internal_report = report
        return self.internal_report

    def convert_to_gitlab(self):
        """
        This function convert the pylint 'parseable' report
        to a gitlab report.
        The pylint 'parseable' report should follow the message convention
        `{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}`
        like define in the doc https://docs.pylint.org/en/1.6.0/output.html

        The output follow the current Gitlab Convntion which is
        the Code Climate Spec like specified on the GitLab page.
        https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html

        For example, please look at the fixture blah.json in the tests folder

        :rtype: str
        :return: return the report in the desired format
        """
        # Define Mapping between Pylint and Code Climate
        # Pylint
        # http://pylint.pycqa.org/en/1.6.0/tutorial.html?highlight=Refactor
        # Code Climate
        # https://docs.codeclimate.com/docs/grep#section-configuring-the-plugin
        categories = {
            "C": ["Style"],
            "R": ["Complexity"],
            "W": ["Bug Risk"],
            "E": ["Bug Risk"],
            "F": ["Bug Risk"],
        }
        severities = {
            "C": "info",
            "R": "info",
            "W": "minor",
            "E": "critical",
            "F": "critical",
        }
        # Parse the pylint report and init self.internal_report
        self.parse_pylint_report()
        # Init the report
        report = []
        for path in self.internal_report:
            numbers = self.internal_report[path].keys()
            for number in numbers:
                for line_msg in self.internal_report[path][number]:
                    column_number = line_msg["column_number"]
                    # Gen a fingerprint based on the path, line, column, msg id
                    fingerprint_str = "{path}_{number}_{col}_{msg_id}".format(
                        path=path,
                        number=number,
                        col=column_number,
                        msg_id=line_msg["msg_id"],
                    )
                    fingerprint = hashlib.md5(fingerprint_str).hexdigest()

                    # Init line report
                    description = "{} {} {}".format(
                        line_msg["msg_id"], line_msg["symbol"], line_msg["obj"]
                    )
                    line_report = {
                        "categories": categories[line_msg["msg_id"][0]],
                        "check_name": "method_complexity",
                        "content": {"body": line_msg["msg"]},
                        "description": description,
                        "fingerprint": fingerprint,
                        "location": {
                            "path": path,
                            "positions": {
                                "begin": {
                                    "line": number,
                                    "column": column_number,
                                },
                                "end": {
                                    "line": number,
                                    "column": column_number,
                                },
                            },
                        },
                        "other_locations": [],
                        "remediation_points": 1850000,
                        "severity": severities[line_msg["msg_id"][0]],
                        "type": "issue",
                        "engine_name": "structure",
                    }
                    report.append(line_report)
        self.output_report = report
        return self.output_report
