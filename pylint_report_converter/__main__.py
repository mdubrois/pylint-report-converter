"""
This file is dedicated to the executable
"""
import sys
import os
import argparse
import pylint_report_converter

if __name__ == "__main__":
    ARGV = sys.argv[1:]
    # Get the arguments
    PARSER = argparse.ArgumentParser(description="", epilog="")
    PARSER.add_argument(
        "-i", "--input_file", required=True, help="The Pylint input file path."
    )
    PARSER.add_argument(
        "-o", "--output_file", required=True, help="The output file path."
    )
    PARSER.add_argument(
        "-f", "--format", choices=["gitlab"], help="The output format."
    )
    ARGS = PARSER.parse_args(ARGV)

    CONVERTER = pylint_report_converter.Converter(
        input_file=os.path.abspath(ARGS.input_file),
        output_file=os.path.abspath(ARGS.output_file),
    )
    RESULT = CONVERTER.convert()
    exit(RESULT)
