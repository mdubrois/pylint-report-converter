"""
This file test the main function
"""
import os
import pylint_report_converter as prc

CURRENT_FOLDER = os.path.dirname(os.path.abspath(__file__))
FIXTURES_FOLDER = os.path.join(CURRENT_FOLDER, "fixtures")


def test_gitlab_report():
    """
    This function test the gitlab report

    :param (str) arg: arg do

    :rtype: str
    :return: return a
    """
    input_file = os.path.join(FIXTURES_FOLDER, "pylint_report.txt")
    output_file = os.path.join(CURRENT_FOLDER, "gitlab_report.json")
    # Get converter
    conv = prc.Converter(
        input_file=input_file, output_file=output_file, output_format="gitlab"
    )
    conv.read_file()
    report = conv.convert_to_gitlab()
    expectedReport = [
        {
            "description": "C0111 missing-docstring ",
            "check_name": "method_complexity",
            "remediation_points": 1850000,
            "fingerprint": "0cbb6bf7f088d1038f5577291158a28b",
            "categories": ["Style"],
            "severity": "info",
            "engine_name": "structure",
            "other_locations": [],
            "content": {"body": "Missing module docstring"},
            "location": {
                "path": "node_graph/__init__.py",
                "positions": {
                    "begin": {"column": "0", "line": "1"},
                    "end": {"column": "0", "line": "1"},
                },
            },
            "type": "issue",
        },
        {
            "description": "R0903 too-few-public-methods Main",
            "check_name": "method_complexity",
            "remediation_points": 1850000,
            "fingerprint": "62a6ccf931ff65a47bde96dfdfbb0279",
            "categories": ["Complexity"],
            "severity": "info",
            "engine_name": "structure",
            "other_locations": [],
            "content": {"body": "Too few public methods (0/2)"},
            "location": {
                "path": "node_graph/__init__.py",
                "positions": {
                    "begin": {"column": "0", "line": "19"},
                    "end": {"column": "0", "line": "19"},
                },
            },
            "type": "issue",
        },
        {
            "description": "W0403 relative-import ",
            "check_name": "method_complexity",
            "remediation_points": 1850000,
            "fingerprint": "1a8a1fc3e010e18545e75d80668bdb28",
            "categories": ["Bug Risk"],
            "severity": "minor",
            "engine_name": "structure",
            "other_locations": [],
            "content": {
                "body": "Relative import 'core.app', should be 'node_graph.core.app'"
            },
            "location": {
                "path": "node_graph/__init__.py",
                "positions": {
                    "begin": {"column": "0", "line": "12"},
                    "end": {"column": "0", "line": "12"},
                },
            },
            "type": "issue",
        },
        {
            "description": "W0613 unused-argument Main.__init__",
            "check_name": "method_complexity",
            "remediation_points": 1850000,
            "fingerprint": "99ab7d0fe8590111a8e14b11e5a483e4",
            "categories": ["Bug Risk"],
            "severity": "minor",
            "engine_name": "structure",
            "other_locations": [],
            "content": {"body": "Unused argument 'gui'"},
            "location": {
                "path": "node_graph/__init__.py",
                "positions": {
                    "begin": {"column": "0", "line": "22"},
                    "end": {"column": "0", "line": "22"},
                },
            },
            "type": "issue",
        },
    ]
    assert report == expectedReport
